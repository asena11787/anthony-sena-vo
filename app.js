var express = require('express');
var app = express();

var port = process.env.PORT || 3000;

app.use(express.static('public'));
app.use(express.static('public/css'));
app.use(express.static('public/images'));
app.use(express.static('public/audio'));
app.use(express.static('public/js'));

// set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

// index page 
app.get('/', function(req, res) {
    res.render('./index');
});

//app.get('/', function(req, res){
//	res.send('Welcome to my API!');
//});

app.listen(port, function(){
	console.log('Gulp is running my app on port: ' + port);
})